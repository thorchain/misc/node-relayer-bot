# bot.py
import os
import sys
import logging
import uuid
import base64
import hashlib
import ecdsa
import json
from aiohttp import web
from datetime import datetime

from thornode import THORNode
from util import asset1e8
from segwit_addr import address_from_public_key

import discord
from discord.ext import commands, tasks
from dotenv import load_dotenv

# Init logging
logging.basicConfig(
    format="%(asctime)s | %(levelname).4s | %(message)s",
    level=os.environ.get("LOGLEVEL", "INFO"),
)

# app = Flask(__name__)
app = web.Application()
routes = web.RouteTableDef()

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
DEBUG = os.getenv('DEBUG').lower() == 'true'
channels = {
    "thornode": 839002272654426122, # thornode dev channel
    "thornode-mainnet": 839001804812451873, # thornode-mainnet
    "mainnet": 839002619481554955, # mainnet
    "test": 859434737034985505, # test guild channel
}

# all UUIDs must have this prefix, which is randomly generated on startup. This
# is used to protect the bot from replay attacks between restarts.
uuid_prefix = str(uuid.uuid1()).split("-")[0]
used_uuid = {} # track already used uuids. This is protect against replay attacks

logging.info("Starting Node Relayer Bot...")

bot = commands.Bot(command_prefix=";", description="Node operator relay bot")

class Webserver(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.web_server.start()

        @routes.get('/uuid_prefix')
        async def prefix(request):
            return web.Response(text=uuid_prefix)

        @routes.post('/msg')
        async def msg(request):
            thor = THORNode()
            data = await request.json()

            sig = data["signature"]
            pubkey = data["pubkey"]

            msg = data["msg"]
            msg_uuid = msg["uuid"]
            text = msg["text"]
            chan = msg["channel"]
            thor_address = None

            # validate json
            if "signature" not in data: # ensure we have a signature
                return web.HTTPBadRequest(text=json.dumps({"error":"must provide 'signature' field"}))
            if "pubkey" not in data: # ensure we have a pubkey
                return web.HTTPBadRequest(text=json.dumps({"error":"must provide 'pubkey' field"}))
            if "msg" not in data: # ensure we have a msg
                return web.HTTPBadRequest(text=json.dumps({"error":"must provide 'msg' field"}))
            if "text" not in msg: # ensure we have an text
                return web.HTTPBadRequest(text=json.dumps({"error":"must provide 'msg.text' field"}))
            if len(text) > 2000:
                return web.HTTPBadRequest(text=json.dumps({"error":"text cannot be longer than 2,000 characters"}))
            if "channel" not in msg: # ensure we have a channel
                return web.HTTPBadRequest(text=json.dumps({"error":"must provide 'msg.channel' field"}))
            if chan not in channels:
                return web.HTTPBadRequest(text=json.dumps({"error":"invalid channel name"}))
            if "uuid" not in msg: # ensure we have a uuid
                return web.HTTPBadRequest(text=json.dumps({"error":"must provide 'uuid' field"}))

            # AUTHENTICATION & AUTHORIZATION
            if not DEBUG:
                if not msg_uuid.startswith(uuid_prefix):
                    return web.HTTPBadRequest(text=json.dumps({"error":"invalid uuid prefix"}))
                if msg_uuid in used_uuid:
                    return web.HTTPBadRequest(text=json.dumps({"error":"uuid already used"}))
                used_uuid[msg_uuid] = True

                # verify signature
                buf = str.encode(f"{msg_uuid}|{chan}|{text}")
                public_key = base64.b64decode(pubkey).hex()
                siggy = base64.b64decode(sig).hex()

                vk = ecdsa.VerifyingKey.from_string(bytes.fromhex(public_key), curve=ecdsa.SECP256k1, hashfunc=hashlib.sha256) # the default is sha1
                if not vk.verify(bytes.fromhex(siggy), buf):
                    return web.HTTPBadRequest(text=json.dumps({"error":"bad signature"}))
            
                thor_address = address_from_public_key(base64.b64decode(pubkey), "thor")

            # get node data
            try:
                node = thor.get_node(thor_address)
                if "error" in node: # check we got back a node ok
                    return web.HTTPBadRequest(text=json.dumps(node))
            except:
                return web.HTTPBadRequest(text=json.dumps({"error":"unrecognized node address"}))

            if not DEBUG:
                mimir = thor.get_mimir('MINIMUMBONDINRUNE')
                if int(node['total_bond']) < mimir:
                    return web.HTTPForbidden(text=json.dumps({"error": "not bonded with enough rune to meet minimum requirements"}))

            # create embed object
            embed = discord.Embed(title=f"Message from {thor_address}", colour=0x87CEEB, timestamp=datetime.utcnow())
            embed.add_field(name="Status", value=node['status'], inline=True)
            embed.add_field(name="Bond", value=f"{int((int(node['total_bond']) / 1e8)):,.0f} Rune", inline=True)
            embed.add_field(name="Message", value=text, inline=False)

            # broadcast embed to discord
            channel = self.bot.get_channel(channels[chan])
            if channel is None:
                return web.HTTPBadRequest(text=json.dumps({"error": "unable to find channel"}))
            await channel.send(embed=embed, delete_after=604800) # deletes after 1 week

            return web.Response(text=json.dumps({"status": "ok!"}))

        self.webserver_port = os.environ.get('PORT', 5000)
        app.add_routes(routes)

    @tasks.loop()
    async def web_server(self):
        runner = web.AppRunner(app)
        await runner.setup()
        site = web.TCPSite(runner, host='0.0.0.0', port=self.webserver_port)
        await site.start()

    @web_server.before_loop
    async def web_server_before_loop(self):
        await self.bot.wait_until_ready()

if __name__ == '__main__':
    bot.add_cog(Webserver(bot))
    bot.run(TOKEN)
