discord==1.7.3
python-dotenv==0.18.0
requests==2.25.1
psycopg2==2.8.6
gunicorn==20.0.4
ecdsa==0.17.0
